package br.com.customtech.service;

import br.com.customtech.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */
@Path("/service")
public class Service {

    @GET
    @Path("/returnValue")
    @Produces("text/plain")
    public String returnValue() {
        return "deu certo";
    }
    
    @GET
	@Path("/users")
	@Produces("application/json")
	public Response getUsers()
	{
		User user = new User();
		user.setId(1);
		user.setFirstName("Andre");
		user.setLastName("Lobregate");
                
                List<User> list = new ArrayList<User>();
                list.add(user);
                
                user = new User();
		user.setId(2);
		user.setFirstName("Niria");
		user.setLastName("Lobregate");
                list.add(user);
                
                
		return Response.status(200).entity(list).build();
	}

}
